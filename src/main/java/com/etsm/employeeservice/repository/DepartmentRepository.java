package com.etsm.employeeservice.repository;

import com.etsm.employeeservice.model.Department;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DepartmentRepository extends JpaRepository<Department, Long> {
    Optional<Department> findDepartmentByName(String name);
    Optional<Department> findDepartmentById(Long id);
    Boolean existsDepartmentByName(String name);
}
