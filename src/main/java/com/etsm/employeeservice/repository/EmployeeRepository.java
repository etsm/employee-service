package com.etsm.employeeservice.repository;

import com.etsm.employeeservice.model.AccessRole;
import com.etsm.employeeservice.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    Optional<Employee> findById(Long id);
    List<Employee> findEmployeesByAccessRoleSet(AccessRole accessRole);
}
