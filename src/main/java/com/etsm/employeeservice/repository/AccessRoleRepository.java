package com.etsm.employeeservice.repository;

import com.etsm.employeeservice.model.AccessRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AccessRoleRepository extends JpaRepository<AccessRole, Long> {
    Optional<AccessRole> findAccessRoleByName(String name);
    Optional<AccessRole> findById(Long id);
}
