package com.etsm.employeeservice.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Objects;

/**
 * headOfDepartment!
 * isRoot!
 * id!
 * displayName
 */

@Table(name = "department")
@Entity
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String name;

    @OneToOne(fetch = FetchType.EAGER)
    @JsonBackReference
    private Employee headOfDepartment;

    public Department(Long id, String name, Employee headOfDepartment) {
        this.id = id;
        this.name = name;
        this.headOfDepartment = headOfDepartment;
    }

    public Department(String name) {
        this.name = name;
    }

    public Department() {
    }


    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Employee getHeadOfDepartment() {
        return headOfDepartment;
    }

    public void setHeadOfDepartment(Employee headOfDepartment) {
        this.headOfDepartment = headOfDepartment;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(headOfDepartment, that.headOfDepartment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, headOfDepartment);
    }
}
