package com.etsm.employeeservice.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.*;

/**
 * firstName!
 * secondName!
 * patronymic
 * email!
 * accessToSystem!
 * department!
 * accessRoles!
 * headOfDepartment
 */

@Entity
@Table(name = "employees")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name ="login_id", unique = true)
    private Long loginId;

    @Column(name = "access_to_system", columnDefinition = "boolean default false", nullable = false)
    private Boolean accessToSystem;

    @Column(name = "first_name")
    private String firstName;


    @Column(name = "second_name", nullable = false)
    private String secondName;

    @Column(name = "patronymic")
    private String patronymic;

    @Email
    @Column(name = "email")
    private String email;

    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable(
            name = "employee_roles",
            joinColumns = @JoinColumn(name = "employee_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<AccessRole> accessRoleSet = new HashSet<>();

    @ManyToOne
    private Department department;

    public Employee() {
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Long getId() {
        return id;
    }

    public Boolean getAccessToSystem() {
        return accessToSystem;
    }

    public void setAccessToSystem(Boolean accessToSystem) {
        this.accessToSystem = accessToSystem;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<AccessRole> getAccessRoleSet() {
        return accessRoleSet;
    }

    public void addAccessRole(AccessRole accessRole) {
        this.accessRoleSet.add(accessRole);
    }

    public void setAccessRoles(Set<AccessRole> accessRoleSet) {
        this.accessRoleSet.addAll(accessRoleSet);
    }

    public Long getLoginId() {
        return this.loginId;
    }

    public void setLoginId(Long loginId) {
        this.loginId = loginId;
    }

    public void setAccessRoleSet(Set<AccessRole> accessRoleSet) {
        this.accessRoleSet = accessRoleSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id) && Objects.equals(loginId, employee.loginId) && Objects.equals(accessToSystem, employee.accessToSystem) && Objects.equals(firstName, employee.firstName) && Objects.equals(secondName, employee.secondName) && Objects.equals(patronymic, employee.patronymic) && Objects.equals(email, employee.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, loginId, accessToSystem, firstName, secondName, patronymic, email);
    }

}
