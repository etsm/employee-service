package com.etsm.employeeservice.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;

@Entity
@Table(name = "access_roles")
public class AccessRole  {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    @NotNull
    private String name;

    @ManyToMany(mappedBy = "accessRoleSet", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    private Set<Employee> employees = new HashSet<>();

    public AccessRole(Long id, String name, Set<Employee> employeeSet) {
        this.id = id;
        this.name = name;
        this.employees = employeeSet;
    }

    public AccessRole(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public AccessRole(String name) {
        this.name = name;
    }

    public AccessRole() {
    }

    public Set<Employee> getEmployeeSet() {
        return employees;
    }

    public void setEmployeeSet(Set<Employee> employeeSet) {
        this.employees = employeeSet;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccessRole that = (AccessRole) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
