package com.etsm.employeeservice.utils;

public class Const {
    public static String GENERAL_ACCESS_ROLE = "Administrator";
    public static String ROOT_DEPARTMENT = "Root";
}
