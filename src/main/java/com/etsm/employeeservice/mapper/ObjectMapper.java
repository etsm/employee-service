package com.etsm.employeeservice.mapper;

import DTO.AccessRoleDTO;
import DTO.DepartmentDTO;
import DTO.EmployeeDTO;
import DTO.LoginDTO;
import com.etsm.employeeservice.model.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface ObjectMapper {
    @Mappings({
            @Mapping(target = "department.headOfDepartment", ignore = true),
            @Mapping(target = "accessRoleSet.employees", ignore = true),
    })
    EmployeeDTO convert(Employee employee);
    List<EmployeeDTO> convertEmployeeListToEmployeeDTOList(List<Employee> employees);
    @Mappings({
            @Mapping(target = "department.headOfDepartment", ignore = true),
            @Mapping(target = "accessRoleSet.employees", ignore = true),
    })
    Employee convert(EmployeeDTO employeeDTO);
    List<Employee> convertEmployeeDTOListToEmployeeList(List<EmployeeDTO> employees);
    @Mappings({
            @Mapping(target = "department.headOfDepartment", ignore = true),    })
    Department convert(DepartmentDTO departmentDTO);
    List<DepartmentDTO> convertDepartmentListToDepartmentDTOList(List<Department> departments);
    DepartmentDTO convert(Department department);
    List<Department> convertDepartmentDTOListToDepartmentList(List<DepartmentDTO> departments);
    @Mappings({
            @Mapping(target = "employees.accessRoleSet", ignore = true),
    })
    AccessRole convert(AccessRoleDTO accessRoleDTO);
    @Mappings({
            @Mapping(target = "employees.accessRoleSet", ignore = true),
    })
    List<AccessRoleDTO> convertAccessRoleListToAccessRoleDTOList(List<AccessRole> accessRoles);
    AccessRoleDTO convert(AccessRole accessRole);
    @Mappings({
            @Mapping(target = "employees.accessRoleSet", ignore = true),
    })
    List<AccessRole> convertAccessRoleDTOListToAccessRoleList(List<AccessRoleDTO> accessRoles);
    @Mappings({
            @Mapping(target = "accessRoleSet.employees", ignore = true),
    })
    Employee update(EmployeeDTO employeeDTO, @MappingTarget Employee employee);
    Department update(DepartmentDTO departmentDTO, @MappingTarget Department department);
    @Mappings({
            @Mapping(target = "employees.accessRoleSet", ignore = true),
    })
    AccessRole update(AccessRoleDTO accessRoleDTO, @MappingTarget AccessRole accessRole);
}
