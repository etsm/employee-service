package com.etsm.employeeservice.service.department;


import DTO.DepartmentDTO;
import DTO.EmployeeDTO;
import exceptions.NotFoundException;

import java.util.List;

public interface DepartmentService {
    DepartmentDTO createDepartment(DepartmentDTO departmentDTO) throws NotFoundException;
    Boolean remove(List<Long> ids);
    DepartmentDTO updateDepartment(DepartmentDTO departmentDTO) throws NotFoundException;
    List<DepartmentDTO> getDepartments();
    DepartmentDTO getDepartmentById(Long id) throws NotFoundException;
    EmployeeDTO getHeadOfDepartment(Long id) throws NotFoundException;
}
