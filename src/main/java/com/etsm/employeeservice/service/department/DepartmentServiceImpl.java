package com.etsm.employeeservice.service.department;

import DTO.DepartmentDTO;
import DTO.EmployeeDTO;
import com.etsm.employeeservice.mapper.ObjectMapper;
import com.etsm.employeeservice.model.Department;
import com.etsm.employeeservice.model.Employee;
import com.etsm.employeeservice.repository.DepartmentRepository;
import exceptions.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentServiceImpl implements DepartmentService {
    private final DepartmentRepository departmentRepository;
    private final ObjectMapper objectMapper;

    public DepartmentServiceImpl(DepartmentRepository departmentRepository, ObjectMapper objectMapper) {
        this.departmentRepository = departmentRepository;
        this.objectMapper = objectMapper;
    }

    @Override
    public DepartmentDTO createDepartment(DepartmentDTO departmentDTO) throws NotFoundException {
        Boolean existsDepartmentByName  = this.departmentRepository.existsDepartmentByName(departmentDTO.getName());

        if(existsDepartmentByName){
            throw new NotFoundException();
        }

        Department departmentCandidate =  this.objectMapper.convert(departmentDTO);
        this.departmentRepository.saveAndFlush(departmentCandidate);


        return this.objectMapper.convert(departmentCandidate);
    }

    @Override
    public Boolean remove(List<Long> ids) {
        List<Department> departmentList = this.departmentRepository.findAllById(ids);
        this.departmentRepository.deleteAll(departmentList);

        return true;
    }

    @Override
    public DepartmentDTO updateDepartment(DepartmentDTO departmentDTO) throws NotFoundException {
        Department department = this.departmentRepository.findDepartmentById(departmentDTO.getId())
                .orElseThrow(NotFoundException::new);
        Department updatedDepartmentCandidate = this.objectMapper.update(departmentDTO, department);
        Department updatedDepartment = this.departmentRepository.saveAndFlush(updatedDepartmentCandidate);
        return this.objectMapper.convert(updatedDepartment);
    }

    @Override
    public List<DepartmentDTO> getDepartments() {
        List<Department> departments = this.departmentRepository.findAll();
        return this.objectMapper.convertDepartmentListToDepartmentDTOList(departments);
    }

    @Override
    public DepartmentDTO getDepartmentById(Long id) throws NotFoundException {
        Department department = this.departmentRepository.findDepartmentById(id).orElseThrow(NotFoundException::new);
        return this.objectMapper.convert(department);
    }

    @Override
    public EmployeeDTO getHeadOfDepartment(Long id) throws NotFoundException {
        Department department = this.departmentRepository.findDepartmentById(id).orElseThrow(NotFoundException::new);
        Employee headOfDepartment = department.getHeadOfDepartment();
        return this.objectMapper.convert(headOfDepartment);
    }
}
