package com.etsm.employeeservice.service.employee;

import DTO.*;
import com.etsm.employeeservice.feign.AuthServiceProxy;
import com.etsm.employeeservice.mapper.ObjectMapper;
import com.etsm.employeeservice.model.AccessRole;
import com.etsm.employeeservice.model.Department;
import com.etsm.employeeservice.model.Employee;
import com.etsm.employeeservice.repository.AccessRoleRepository;
import com.etsm.employeeservice.repository.DepartmentRepository;
import com.etsm.employeeservice.repository.EmployeeRepository;
import com.etsm.employeeservice.utils.Const;
import exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final AccessRoleRepository accessRoleRepository;
    private final DepartmentRepository departmentRepository;
    private final ObjectMapper objectMapper;
    private final AuthServiceProxy authServiceProxy;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository,
                               AccessRoleRepository accessRoleRepository,
                               DepartmentRepository departmentRepository,
                               ObjectMapper objectMapper,
                               AuthServiceProxy authServiceProxy) {
        this.employeeRepository = employeeRepository;
        this.accessRoleRepository = accessRoleRepository;
        this.departmentRepository = departmentRepository;
        this.objectMapper = objectMapper;
        this.authServiceProxy = authServiceProxy;
    }

    private Employee updateEmployeeFields(EmployeeDTO employeeDTO, Employee employee) throws NotFoundException {
        if(employee == null || employeeDTO == null){
            return null;
        }

        if(employeeDTO.getAccessToSystem() != null){
            employee.setAccessToSystem(employeeDTO.getAccessToSystem());
        }

        if(employeeDTO.getFirstName() != null){
            employee.setFirstName(employeeDTO.getFirstName());
        }

        if(employeeDTO.getDepartment() != null){
            DepartmentDTO departmentDTO = employeeDTO.getDepartment();
            Department department = this.departmentRepository
                    .findDepartmentById(departmentDTO.getId())
                    .orElseThrow(NotFoundException::new);

            employee.setDepartment(department);
        }
        employee.setEmail(employeeDTO.getEmail());
        employee.setPatronymic(employeeDTO.getPatronymic());
        employee.setSecondName(employeeDTO.getSecondName());
        Set<AccessRole> accessRoleSet = employeeDTO.getAccessRoleSet().stream().map(
                role -> {
                    try {
                        //todo: Переделать на использование сервиса ролей
                        return this.accessRoleRepository.findById(role.getId()).orElseThrow(NotFoundException::new);
                    } catch (NotFoundException e) {
                        e.printStackTrace();
                        return null;
                    }
                })
                .collect(Collectors.toSet());
        employee.setAccessRoleSet(accessRoleSet);

        return employee;
    }

    @Override
    public EmployeeDTO updateEmployee(EmployeeDTO employeeDTO) throws Exception {
        if (employeeDTO == null) {
            throw new Exception("Can not be null");
        }

        Employee employee = this.employeeRepository.findById(employeeDTO.getId())
                .orElseThrow(NotFoundException::new);
        DepartmentDTO departmentDTO = employeeDTO.getDepartment();

        if(departmentDTO != null){
            Department department = this.departmentRepository.findDepartmentById(departmentDTO.getId())
                    .orElseThrow(NotFoundException::new);
            employee.setDepartment(department);
            DepartmentDTO departmentDTO1 = this.objectMapper.convert(department);
            employeeDTO.setDepartment(departmentDTO1);
        }
        if(employeeDTO.getLogin() == null){
            this.authServiceProxy.updateLogin(employee.getLoginId().toString(), employeeDTO.getLogin());
        }

        Employee updatedEmployeeCandidate = this.updateEmployeeFields(employeeDTO, employee);

        Employee updatedEmployee = this.employeeRepository.saveAndFlush(updatedEmployeeCandidate);
        LoginDTO updatedLogin = this.authServiceProxy.getLogin(updatedEmployee.getId().toString());
        EmployeeDTO loginDTO = this.objectMapper.convert(updatedEmployee);
        loginDTO.setLogin(updatedLogin.getLogin());

        return loginDTO;
    }

    private void initialize(){
        List<AccessRole> accessRoleList = this.accessRoleRepository.findAll();
        List<Department> departmentList = this.departmentRepository.findAll();
        if(accessRoleList.isEmpty()){
            AccessRole accessRole = new AccessRole(Const.GENERAL_ACCESS_ROLE);
            this.accessRoleRepository.saveAndFlush(accessRole);
        }

        if(departmentList.isEmpty()){
            Department department = new Department(Const.ROOT_DEPARTMENT);
            this.departmentRepository.saveAndFlush(department);
        }

    }

    @Override
    public EmployeeDTO firstUser(LoginDTO employeeDTO) throws Exception {
        if(
                this.employeeRepository.count() > 0
                        || this.accessRoleRepository.count() > 0
                        || this.departmentRepository.count() > 0
        ){
            throw new Exception("Init system already completed");
        }

        if(employeeDTO == null){
            throw new Exception("Pair login and password is not exist");
        }

        String loginWord = employeeDTO.getLogin();
        String password = employeeDTO.getPassword();

        if(loginWord == null || password == null || loginWord.equals("") || password.equals("")){
            throw new Exception("Pair login or password is empty");
        }

        this.initialize();

        Optional<AccessRole> accessRole= this.accessRoleRepository.findAccessRoleByName(Const.GENERAL_ACCESS_ROLE);
        Department department = this.departmentRepository.findDepartmentByName(Const.ROOT_DEPARTMENT)
                .orElseThrow(NotFoundException::new);
        Employee employee = new Employee();
        employee.setFirstName(employeeDTO.getFirstName());
        employee.setEmail(employeeDTO.getEmail());
        employee.setSecondName(employeeDTO.getSecondName());
        employee.setAccessToSystem(true);

        if(department != null){
            employee.setDepartment(department);
            department.setHeadOfDepartment(employee);
        }

        accessRole.ifPresent(employee::addAccessRole);        Employee createdEmployee  = this.employeeRepository.saveAndFlush(employee);

        employeeDTO.setId(createdEmployee.getId());
        Long loginId = this.authServiceProxy.addUser(employeeDTO);
        employee.setLoginId(loginId);
        Employee resultEmployee  = this.employeeRepository.saveAndFlush(employee);
        LoginDTO updatedLogin = this.authServiceProxy.getLogin(resultEmployee.getId().toString());
        EmployeeDTO loginDTO = this.objectMapper.convert(resultEmployee);
        loginDTO.setLogin(updatedLogin.getLogin());

        return loginDTO;
    }

    @Override
    public EmployeeDTO createEmployee(LoginDTO employeeDTO) throws Exception {

        if(employeeDTO == null){
            throw new Exception("Pair login and password is not exist");
        }

        String loginWord = employeeDTO.getLogin();

        if(loginWord == null || loginWord.equals("")){
            throw new Exception("Pair login or password is empty");
        }

        Employee employee = this.objectMapper.convert(employeeDTO);
        if(employeeDTO.getDepartment() == null){
            Department rootDepartment = this.departmentRepository.findDepartmentByName("Root").orElseThrow(NotFoundException::new);
            employee.setDepartment(rootDepartment);
        } else {
            DepartmentDTO departmentDTO = employeeDTO.getDepartment();
            Department department = this.departmentRepository.findDepartmentById(departmentDTO.getId())
                    .orElseThrow(NotFoundException::new);
            employee.setDepartment(department);
        }

        Employee createdEmployee = this.employeeRepository.saveAndFlush(employee);
        employeeDTO.setId(createdEmployee.getId());
        Long loginId = this.authServiceProxy.addUser(employeeDTO);
        createdEmployee.setLoginId(loginId);
        createdEmployee = this.employeeRepository.saveAndFlush(employee);
        LoginDTO updatedLogin = this.authServiceProxy.getLogin(createdEmployee.getId().toString());
        EmployeeDTO loginDTO = this.objectMapper.convert(createdEmployee);
        loginDTO.setLogin(updatedLogin.getLogin());

        return loginDTO;
    }

    @Override
    public List<EmployeeDTO> getEmployees() {
        List<Employee> employees = this.employeeRepository.findAll();
        return this.objectMapper.convertEmployeeListToEmployeeDTOList(employees);
    }

    @Override
    public EmployeeDTO getEmployeeById(Long id) throws NotFoundException {
        Employee employee = this.employeeRepository.findById(id).orElseThrow(NotFoundException::new);
        LoginDTO login = this.authServiceProxy.getLogin(employee.getId().toString());

        return EmployeeDTO.builder()
                .id(employee.getId())
                .login(login.getLogin())
                .firstName(employee.getFirstName())
                .secondName(employee.getSecondName())
                .patronymic(employee.getPatronymic())
                .email(employee.getEmail())
                .department(this.objectMapper.convert(employee.getDepartment()))
                .accessRoleSet(
                        new HashSet<>(
                                this.objectMapper.convertAccessRoleListToAccessRoleDTOList(
                                        new ArrayList<>(employee.getAccessRoleSet())
                                )
                        )
                ).build();
    }

    @Override
    public DepartmentDTO getEmployeeDepartment(Long id) throws NotFoundException {
        Employee employee = this.employeeRepository.findById(id).orElseThrow(NotFoundException::new);
        return this.objectMapper.convert(employee.getDepartment());
    }

    @Override
    public Set<AccessRoleDTO> getEmployeeAccessRoles(Long id) throws NotFoundException {
        Employee employee = this.employeeRepository.findById(id).orElseThrow(NotFoundException::new);
        EmployeeDTO employeeDTO = this.objectMapper.convert(employee);
        return employeeDTO.getAccessRoleSet();
    }

    @Override
    public Boolean remove(List<Long> ids) {
        List<Employee> employees = this.employeeRepository.findAllById(ids);
        List<Long> loginIds = employees.stream().map(Employee::getLoginId).collect(Collectors.toList());
        this.employeeRepository.deleteAll(employees);
        this.authServiceProxy.remove(new ListIdsDTO(loginIds));
        return true;
    }
}
