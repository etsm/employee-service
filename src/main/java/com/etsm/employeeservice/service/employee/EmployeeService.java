package com.etsm.employeeservice.service.employee;

import DTO.*;
import exceptions.NotFoundException;

import java.util.List;
import java.util.Set;

public interface EmployeeService {
    EmployeeDTO updateEmployee(EmployeeDTO employeeDTO) throws NotFoundException, Exception;
    EmployeeDTO firstUser(LoginDTO loginDTO) throws Exception;
    EmployeeDTO createEmployee(LoginDTO employeeDTO) throws Exception;
    List<EmployeeDTO> getEmployees();
    EmployeeDTO getEmployeeById(Long id) throws NotFoundException;
    DepartmentDTO getEmployeeDepartment(Long id) throws NotFoundException;
    Set<AccessRoleDTO> getEmployeeAccessRoles(Long id) throws NotFoundException;
    Boolean remove(List<Long> ids);
}
