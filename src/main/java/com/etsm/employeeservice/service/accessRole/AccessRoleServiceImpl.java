package com.etsm.employeeservice.service.accessRole;

import DTO.AccessRoleDTO;
import com.etsm.employeeservice.mapper.ObjectMapper;
import com.etsm.employeeservice.model.AccessRole;
import com.etsm.employeeservice.model.Employee;
import com.etsm.employeeservice.repository.AccessRoleRepository;
import com.etsm.employeeservice.repository.EmployeeRepository;
import exceptions.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AccessRoleServiceImpl implements AccessRoleService{

    private final AccessRoleRepository accessRoleRepository;
    private final ObjectMapper objectMapper;
    private final EmployeeRepository employeeRepository;

    public AccessRoleServiceImpl(AccessRoleRepository accessRoleRepository, ObjectMapper objectMapper, EmployeeRepository employeeRepository) {
        this.accessRoleRepository = accessRoleRepository;
        this.objectMapper = objectMapper;
        this.employeeRepository = employeeRepository;
    }

    public AccessRole updateRoleFields(AccessRole target, AccessRoleDTO source) {
        if(source == null){
            return target;
        }

        target.setName(source.getName());
        Set<Employee> employees = null;

        if(target.getEmployeeSet() != null){
            employees = source.getEmployees().stream()
                    .map(employee -> {

                        Employee targetEmployee = this.employeeRepository.findById(employee.getId()).orElseThrow();
                        targetEmployee.addAccessRole(target);

                        return this.employeeRepository.saveAndFlush(targetEmployee);
                    })
                    .collect(Collectors.toSet());
        }

        target.setEmployeeSet(employees);

        return target;
    }

    @Override
    public AccessRoleDTO createAccessRole(AccessRoleDTO accessRoleDTO) {
        AccessRole accessRole = this.objectMapper.convert(accessRoleDTO);

        AccessRole cratedAccessRole = this.accessRoleRepository.saveAndFlush(accessRole);

        return  AccessRoleDTO.builder()
                .id(cratedAccessRole.getId())
                .name(cratedAccessRole.getName())
                .employees(
                        cratedAccessRole.getEmployeeSet()
                                .stream()
                                .map(this.objectMapper::convert)
                                .collect(Collectors.toSet())
                )
                .build();
    }

    @Override
    public AccessRoleDTO updateAccessRole(AccessRoleDTO accessRoleDTO) throws NotFoundException {
        AccessRole accessRole = this.accessRoleRepository.findById(accessRoleDTO.getId()).orElseThrow(NotFoundException::new);
        AccessRole updatedAccessRoleCandidate = this.updateRoleFields(accessRole, accessRoleDTO);

        AccessRole updatedAccessRole = this.accessRoleRepository.saveAndFlush(updatedAccessRoleCandidate);

        return  AccessRoleDTO.builder()
                .id(updatedAccessRole.getId())
                .name(updatedAccessRole.getName())
                .employees(
                        updatedAccessRole.getEmployeeSet()
                                .stream()
                                .map(this.objectMapper::convert)
                                .collect(Collectors.toSet())
                )
                .build();
    }

    @Override
    public Boolean remove(List<Long> ids) {
        List<AccessRole> accessRoleList = this.accessRoleRepository.findAllById(ids);
        this.accessRoleRepository.deleteAll(accessRoleList);
        return true;
    }

    @Override
    public List<AccessRoleDTO> getAccessRoles() {
        List<AccessRole> accessRoles = this.accessRoleRepository.findAll();
        List<AccessRoleDTO> accessRoleDTOList = new ArrayList<>();
       for(AccessRole role : accessRoles) {
           accessRoleDTOList.add(
                   AccessRoleDTO.builder()
                   .id(role.getId())
                   .name(role.getName())
                   .employees(
                           role.getEmployeeSet()
                                   .stream()
                                   .map(this.objectMapper::convert)
                                   .collect(Collectors.toSet())
                   )
                   .build()
           );
       }

        return accessRoleDTOList;
    }

    @Override
    public AccessRoleDTO getAccessRoleById(Long id) throws NotFoundException {
        AccessRole accessRole = this.accessRoleRepository.findById(id).orElseThrow(NotFoundException::new);
        return  AccessRoleDTO.builder()
                .id(accessRole.getId())
                .name(accessRole.getName())
                .employees(
                        accessRole.getEmployeeSet()
                                .stream()
                                .map(this.objectMapper::convert)
                                .collect(Collectors.toSet())
                )
                .build();
    }
}
