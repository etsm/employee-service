package com.etsm.employeeservice.service.accessRole;

import DTO.AccessRoleDTO;
import exceptions.NotFoundException;

import java.util.List;

public interface AccessRoleService {
    AccessRoleDTO createAccessRole(AccessRoleDTO accessRoleDTO);
    AccessRoleDTO updateAccessRole(AccessRoleDTO accessRoleDTO) throws NotFoundException;
    Boolean remove(List<Long> ids);
    List<AccessRoleDTO> getAccessRoles();
    AccessRoleDTO getAccessRoleById(Long id) throws NotFoundException;;
}
