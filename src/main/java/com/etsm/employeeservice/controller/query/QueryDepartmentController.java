package com.etsm.employeeservice.controller.query;

import DTO.DepartmentDTO;
import DTO.EmployeeDTO;
import com.etsm.employeeservice.service.department.DepartmentService;
import exceptions.NotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/query/department")
public class QueryDepartmentController {

    private final DepartmentService departmentService;

    public QueryDepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @GetMapping("/all")
    public List<DepartmentDTO> getDepartments(){
        return this.departmentService.getDepartments();
    }

    @GetMapping("/{id}")
    public DepartmentDTO getDepartmentById(@PathVariable String id) throws NotFoundException {
        return this.departmentService.getDepartmentById(Long.parseLong(id));
    }

    @GetMapping("/headOfDepartment/{departmentId}")
    public EmployeeDTO getHeadOfDepartment(@PathVariable String departmentId) throws NotFoundException {
        return this.departmentService.getHeadOfDepartment(Long.parseLong(departmentId));
    }
}