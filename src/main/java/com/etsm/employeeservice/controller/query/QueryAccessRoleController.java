package com.etsm.employeeservice.controller.query;

import DTO.AccessRoleDTO;
import com.etsm.employeeservice.service.accessRole.AccessRoleService;
import exceptions.NotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/query/access-role")
public class QueryAccessRoleController {

    private final AccessRoleService accessRoleService;

    public QueryAccessRoleController(AccessRoleService accessRoleService) {
        this.accessRoleService = accessRoleService;
    }

    @GetMapping("/all")
    public List<AccessRoleDTO> getAccessRoleList(){
        return this.accessRoleService.getAccessRoles();
    }

    @GetMapping("/{id}")
    public AccessRoleDTO getAccessRoleById(@PathVariable String id) throws NotFoundException {
        return this.accessRoleService.getAccessRoleById(Long.parseLong(id));
    }

}
