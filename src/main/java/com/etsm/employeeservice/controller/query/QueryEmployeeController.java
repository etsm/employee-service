package com.etsm.employeeservice.controller.query;


import DTO.AccessRoleDTO;
import DTO.DepartmentDTO;
import DTO.EmployeeDTO;
import com.etsm.employeeservice.service.employee.EmployeeService;
import exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/query/employee")
public class QueryEmployeeController {

    private final EmployeeService employeeService;

    @Autowired
    public QueryEmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("/all")
    private List<EmployeeDTO> getAllEmployees() {
        return this.employeeService.getEmployees();
    }

    @GetMapping("/{id}")
    public EmployeeDTO getEmployeeById(@PathVariable String id) throws NotFoundException {
        return this.employeeService.getEmployeeById(Long.parseLong(id));
    }

    @GetMapping("/{employeeId}/accessRoles")
    public Set<AccessRoleDTO> getEmployeeAccessRoles(@PathVariable String employeeId ) throws NotFoundException {
        return this.employeeService.getEmployeeAccessRoles(Long.parseLong(employeeId));
    }

    @GetMapping("/{employeeId}/department")
    public DepartmentDTO getEmployeeDepartment(@PathVariable String employeeId ) throws NotFoundException {
        return this.employeeService.getEmployeeDepartment(Long.parseLong(employeeId));
    }

}
