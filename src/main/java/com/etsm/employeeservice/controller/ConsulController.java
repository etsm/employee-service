package com.etsm.employeeservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/consul/")
public class ConsulController {
    @GetMapping("/ruok")
    public ResponseEntity<String> test(){
        return new ResponseEntity<String>("imok", HttpStatus.ACCEPTED);
    }
}
