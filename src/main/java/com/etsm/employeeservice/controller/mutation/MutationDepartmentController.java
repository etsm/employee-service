package com.etsm.employeeservice.controller.mutation;


import DTO.DepartmentDTO;
import DTO.ListIdsDTO;
import com.etsm.employeeservice.service.department.DepartmentService;
import exceptions.NotFoundException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/mutation/department")
public class MutationDepartmentController {
    private final DepartmentService departmentService;

    public MutationDepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    public DepartmentDTO createDepartment(@RequestBody DepartmentDTO departmentDTO) throws NotFoundException {
        return this.departmentService.createDepartment(departmentDTO);
    }

    @DeleteMapping("/remove")
    public Boolean remove(@RequestBody ListIdsDTO ids) {
        return this.departmentService.remove(ids.getIds());
    }

    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public DepartmentDTO updateDepartment(@RequestBody DepartmentDTO departmentDTO) throws NotFoundException {
        return this.departmentService.updateDepartment(departmentDTO);
    }
}
