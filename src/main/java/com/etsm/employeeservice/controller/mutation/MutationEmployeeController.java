package com.etsm.employeeservice.controller.mutation;

import DTO.ComplexPasswordDTO;
import DTO.EmployeeDTO;
import DTO.ListIdsDTO;
import DTO.LoginDTO;
import com.etsm.employeeservice.service.employee.EmployeeService;
import exceptions.NotFoundException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/mutation/employee")
public class MutationEmployeeController {

    private final EmployeeService employeeService;

    public MutationEmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @PostMapping(value = "/initialize", consumes = MediaType.APPLICATION_JSON_VALUE)
    public EmployeeDTO initialize(@RequestBody LoginDTO loginDto) throws Exception {
        return this.employeeService.firstUser(loginDto);
    }

    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public EmployeeDTO updateEmployeeById(@RequestBody EmployeeDTO employeeDTO) throws Exception {
        return this.employeeService.updateEmployee(employeeDTO);
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    public EmployeeDTO createEmployee(@RequestBody LoginDTO employeeDTO) throws Exception {
        return this.employeeService.createEmployee(employeeDTO);
    }

    @DeleteMapping(value = "/remove")
    public Boolean remove(@RequestBody ListIdsDTO ids) {
        return this.employeeService.remove(ids.getIds());
    }
}
