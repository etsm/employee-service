package com.etsm.employeeservice.controller.mutation;

import DTO.AccessRoleDTO;
import DTO.ListIdsDTO;
import com.etsm.employeeservice.service.accessRole.AccessRoleService;
import exceptions.NotFoundException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/mutation/access_role")
public class MutationAccessRoleController {

    private final AccessRoleService accessRoleService;

    public MutationAccessRoleController(AccessRoleService accessRoleService) {
        this.accessRoleService = accessRoleService;
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    public AccessRoleDTO createAccessRole(@RequestBody AccessRoleDTO accessRoleDTO) {
        return this.accessRoleService.createAccessRole(accessRoleDTO);
    }

    @DeleteMapping("/remove")
    public Boolean remove(@RequestBody ListIdsDTO ids) {
        return this.accessRoleService.remove(ids.getIds());
    }

    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public AccessRoleDTO updateAccessRole(@RequestBody AccessRoleDTO accessRoleDTO) throws NotFoundException {
        return this.accessRoleService.updateAccessRole(accessRoleDTO);
    }

}
