package com.etsm.employeeservice;

import com.etsm.employeeservice.model.AccessRole;
import com.etsm.employeeservice.model.Department;
import com.etsm.employeeservice.model.Employee;
import com.etsm.employeeservice.repository.AccessRoleRepository;
import com.etsm.employeeservice.repository.DepartmentRepository;
import com.etsm.employeeservice.repository.EmployeeRepository;
import com.etsm.employeeservice.utils.Const;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@SpringBootTest
class EmployeeServiceApplicationTests {


	@Test
	void contextLoads() {
	}

	private final DepartmentRepository departmentRepository;
	private final AccessRoleRepository accessRoleRepository;
	private final EmployeeRepository employeeRepository;

	@Autowired
	public EmployeeServiceApplicationTests(DepartmentRepository departmentRepository,
										   EmployeeRepository employeeRepository,
										   AccessRoleRepository accessRoleRepository) {
		this.departmentRepository = departmentRepository;
		this.employeeRepository = employeeRepository;
		this.accessRoleRepository = accessRoleRepository;
	}

//	@Test
//	public void departmentRelationships(){
//		Department department = this.departmentRepository.findDepartmentByDisplayName("Root");
//		if(department != null){
//			Employee head = department.getHeadOfDepartment();
//			Assert.notNull(head, "связь не найденв");
//		}
//	}

	@Test
	public void existRoleOnEmployee(){
		List<Employee> employeeList = this.employeeRepository.findAll();
		employeeList.forEach(employee -> {
			Set<AccessRole> accessRoleSet = employee.getAccessRoleSet();
			Assert.notEmpty(accessRoleSet, "У пользователя должна быть роль");
		});
	}

	@Test
	public void existEmployeeWithGeneralAccessRole(){
		Optional<AccessRole> generalAccessRoles = this.accessRoleRepository.findAccessRoleByName(Const.GENERAL_ACCESS_ROLE);
		generalAccessRoles.ifPresent(accessRole -> {
			List<Employee> employeeList = this.employeeRepository.findEmployeesByAccessRoleSet(accessRole);
			Assert.notEmpty(employeeList, "В системе должна быть хотя бы 1 основной администратор");
		});

	}
}
