#ВАЖНО! Запускать требуется из монорепозитория
FROM openjdk:11

#Создаем папку для библиотеки классов
RUN mkdir /DataTransferObjectLib

#Создаем папку для севиса
RUN mkdir /employee-service

#Копируем библиотеку классов в образ
COPY ./DataTransferObjectLib /DataTransferObjectLib/

#Копируем сервис
COPY ./employee-service /employee-service/

#Устанавливаем рабочую дирректорию
WORKDIR /employee-service

#Собираем проект
RUN  ./gradlew clean && ./gradlew build -x test

#Сразу прокидываем нужный порт
EXPOSE 8091

#Создаем дирректорию для запуска
RUN mkdir /app

#Копируем JAR в дирректорию для запуска
RUN cp /employee-service/build/libs/*.jar /app/employee-service.jar

#запуск сервиса
ENTRYPOINT [\
"java",\
"-XX:+UnlockExperimentalVMOptions",\
"-Djava.security.egd=file:/dev/./urandom",\
"-jar","/app/employee-service.jar",\
"--spring.config.location=file:///employee-service/src/main/resources/production.yml"]